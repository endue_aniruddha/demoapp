//
//  HomeViewModelUnitTest.swift
//  MyGadgetsTests
//
//  Created by Endue_Alam on 13/12/21.
//

import XCTest
@testable import MyGadgets

class HomeViewModelUnitTest: XCTestCase {

    var homeviewmodel: HomeViewModel!
    override func setUp() {
        super.setUp()
        homeviewmodel = HomeViewModel()
    }
    
    override func tearDown() {
        homeviewmodel = nil
        super.tearDown()
    }
    
   
}
