//
//  CircularUIImageView.swift
//  OnTracPT
//
//  Created by Endue Alam on 15/10/19.
//  Copyright © 2019 Endue MacBook Pro 15. All rights reserved.
//

import UIKit


@IBDesignable
class CircularUIImageView: UIImageView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor?{
        didSet{
            self.layer.borderColor = self.borderColor?.cgColor
        }
    }
    
    override func prepareForInterfaceBuilder() {
        customizeView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        customizeView()
    }
    
    func customizeView(){
        self.layer.cornerRadius = self.layer.frame.width / 2
        self.layer.masksToBounds = true
    }
    
    
}
