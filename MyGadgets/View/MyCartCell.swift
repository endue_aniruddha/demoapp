

import UIKit

class MyCartCell: UITableViewCell {
    @IBOutlet weak var imgCart: UIImageView!
    @IBOutlet weak var lblCartDeviceName: UILabel!
    @IBOutlet weak var lblcartDevicePrice: UILabel!
    @IBOutlet weak var lblCartDeviceRating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func cartDetails(imageUrl: String, deviceName:String, deviceprice: String, deviceRating: String) {
        lblCartDeviceName.text = deviceName
        lblcartDevicePrice.text = String("Price: \(deviceprice) /-")
        lblCartDeviceRating.text = String("Rating: \(deviceRating)")
        downloaded(from: imageUrl )
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
extension MyCartCell {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
            else { return }
            DispatchQueue.main.async() {
                self.imgCart.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
