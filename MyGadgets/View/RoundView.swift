//
//  RoundView.swift
//  OnTracPT
//
//  Created by Endue Alam on 15/10/19.
//  Copyright © 2019 Endue MacBook Pro 15. All rights reserved.
//

import UIKit

@IBDesignable
class RoundView: UIView {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = self.borderWidth  }}
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            if self.cornerRadius == 0{
                self.layer.cornerRadius = self.layer.frame.height / 2
            }else{
                self.layer.cornerRadius = self.cornerRadius
            }
        }
    }
    @IBInspectable var borderColor:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1){
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    override func prepareForInterfaceBuilder() {
        customizeView()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        customizeView()
    }
    override func layoutSubviews() {
        customizeView()
    }
    func customizeView(){
        
        if self.cornerRadius == 0{
            self.layer.cornerRadius = self.layer.frame.height / 2
        }
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        layer.layoutIfNeeded()
    }
    
}

