//
//  CircleView.swift
//  OnTracPT
//
//  Created by Endue Alam on 21/10/19.
//  Copyright © 2019 Endue MacBook Pro 15. All rights reserved.
//

import UIKit

@IBDesignable
class CircleView: UIView {
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = self.borderWidth
        }
    }
    
    @IBInspectable var borderColor:UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1){
        didSet{
            self.layer.borderColor = self.borderColor.cgColor
        }
    }
    override func prepareForInterfaceBuilder() {
        customize()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        customize()
    }
    override func layoutSubviews() {
        customize()
    }
    func customize(){
        layer.cornerRadius = layer.frame.width / 2
        clipsToBounds = true
    }
    
}
