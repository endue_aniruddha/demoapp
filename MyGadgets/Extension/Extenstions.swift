//
//  Extenstions.swift
//  MR Register
//
//  Created by Endue Aniruddha on 05/11/21.
//

import UIKit

extension UIViewController{
    var preferredStatusBarStyle : UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark{
                return UIStatusBarStyle.lightContent}else{return UIStatusBarStyle.default}}else{
                    return UIStatusBarStyle.default }
    }
    func updateStatusBar(_ viewController:UIViewController){
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(named: "PrimaryColor")
            view.addSubview(statusbarView)
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(named: "PrimaryColor")
        }
    }
}
