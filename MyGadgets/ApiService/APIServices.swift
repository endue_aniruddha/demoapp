

import UIKit

class APIServices: NSObject {
    func callAPInetwork(escapingData: @escaping(HomeModel) -> ()) {
        let url = URL(string: "https://my-json-server.typicode.com/nancymadan/assignment/db")!
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request) { (data, response, error) in
            if let err = error{print("Error\(err)")}else{if
                let data = data
            {
                do{
                    let jsonData = try JSONDecoder().decode(HomeModel.self, from: data)
                    escapingData(jsonData)
                }catch{print(error.localizedDescription)}}else{print("Error\(LocalizedError.self)")}}}
        task.resume()}}
