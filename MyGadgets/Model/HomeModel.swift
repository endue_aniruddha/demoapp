
import Foundation

struct HomeModel:Decodable {
    let products: [products]?
}
// MARK: - Product
struct products:Decodable {
    let name: String?
    let price: String?
    let image_url: String?
    let rating: Int?
    var convertIntprice:Int? {
        let value : Int = Int(price ?? "")!
        return value
    }
}

