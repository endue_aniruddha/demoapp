//
//  CoreDataClass.swift
//  MyGadgets
//
//  Created by Endue_Alam on 11/12/21.
//

import Foundation
import CoreData

extension Product {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Products")
    }
    @NSManaged public var name: String
    @NSManaged public var imageurl: String
    @NSManaged public var price: String
    @NSManaged public var rating: String
}
