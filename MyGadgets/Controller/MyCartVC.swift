
import UIKit
import CoreData
class MyCartVC: UIViewController {
    
    @IBOutlet weak var buttonCheckOut: UIButton!
    @IBOutlet weak var tableMyCart: UITableView!
    var cartViewModel:MyCartViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        updateStatusBar(self)
        cartViewModel = MyCartViewModel()
        tableMyCart.delegate = cartViewModel
        tableMyCart.dataSource = cartViewModel
        tableMyCart.reloadData()
    }
    
    @IBAction func buttonActionCheckOut(_ sender: Any) {
        gotoPementProcessing()
    }
    func gotoPementProcessing() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let CreateProcurement = storyBoard.instantiateViewController(withIdentifier: "TransactionProcessVC") as! TransactionProcessVC
        self.navigationController?.pushViewController(CreateProcurement, animated: true)
    }
    
}
