
import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var homeTable: UITableView!
    @IBOutlet weak var buttonCart: UIButton!
    var viewmodel:HomeViewModel?
    let coreDataManager = PersistentStorage()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        homeTable.delegate = self
        homeTable.dataSource = self
        viewmodel = HomeViewModel()
        viewmodel?.apiHandlerData = {
            self.getApiDataFromViewModel()
        }
        
    }
    func getApiDataFromViewModel(){
        DispatchQueue.main.async {
            self.homeTable.reloadData()
        }
    }
    //CartButton
    @IBAction func cartButtonAction(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let CartProduct = storyBoard.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        self.navigationController?.pushViewController(CartProduct, animated: true)
    }
    
}
extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 111
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        let headerText = UILabel()
        headerText.textColor = UIColor.label
        headerText.adjustsFontSizeToFitWidth = true
        switch section{
        case 0:
            headerText.textAlignment = .center
            headerText.text = "Less Than 1000"
            headerText.font = UIFont.boldSystemFont(ofSize: 25)
        case 1:
            headerText.textAlignment = .center
            headerText.text = "More Than 1000"
            headerText.font = UIFont.boldSystemFont(ofSize: 25)
        default: break
            
        }
        return headerText
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section) {
        case 0:
            return viewmodel?.filterLowPrice()?.count ?? 0
        default:
            return viewmodel?.filterHighPrice()?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as! HomeCell
        if indexPath.section == 0 {
            cell.cellData = viewmodel?.filterLowPrice()?[indexPath.row]
        }else if indexPath.section == 1{
            cell.cellData = viewmodel?.filterHighPrice()?[indexPath.row]
        }else{}
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            guard let selectProduct = viewmodel?.filterLowPrice()?[indexPath.row] else { return }
            gotoMyCartPage(selectedProducts: selectProduct)
        }else
        {
            guard let selectProduct = viewmodel?.filterHighPrice()?[indexPath.row] else { return }
            gotoMyCartPage(selectedProducts: selectProduct)
        }
    }
    
    func gotoMyCartPage(selectedProducts: products) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let cart = Products(context: PersistentStorage.shared.context)
        cart.name = selectedProducts.name ?? ""
        cart.price = selectedProducts.price ?? ""
        cart.rating = String(selectedProducts.rating ?? 0)
        cart.imageurl = selectedProducts.image_url ?? ""
        PersistentStorage.shared.saveContext()
        let CartProduct = storyBoard.instantiateViewController(withIdentifier: "MyCartVC") as! MyCartVC
        self.navigationController?.pushViewController(CartProduct, animated: true)
    }
    
}
