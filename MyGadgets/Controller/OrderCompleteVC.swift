

import UIKit

class OrderCompleteVC: UIViewController {

    @IBOutlet weak var outletContinueShopping: UIButton!
    override func viewDidLoad() {
        updateStatusBar(self)
        navigationItem.hidesBackButton = true
        super.viewDidLoad()
    }
    @IBAction func buttonActionContinueShopping(_ sender: Any) {
         gotoMyHomePage()
    }
    
    func gotoMyHomePage() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let CreateProcurement = storyBoard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(CreateProcurement, animated: true)
    }
}
