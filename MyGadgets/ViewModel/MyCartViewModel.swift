

import Foundation
import CoreData
import UIKit

class MyCartViewModel: NSObject {
    var List: [NSManagedObject] = []
    var context:NSManagedObjectContext!
    override init() {
        super.init()
    }
    func fetchMyCart() ->[NSManagedObject]? {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        debugPrint(path[0])
        do {
            guard let result = try PersistentStorage.shared.context.fetch(Products.fetchRequest()) as? [Products] else { return []}
               List = result
        } catch let error
        {
            debugPrint(error)
        }
        return List
    }
}

extension MyCartViewModel: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 111
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchMyCart()?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCartCell") as! MyCartCell
        let data: NSManagedObject = (fetchMyCart()? [indexPath.row])!
        cell.cartDetails(imageUrl: data.value(forKey: "imageurl") as? String ?? "", deviceName: data.value(forKey: "name") as? String ?? "", deviceprice: data.value(forKey: "price") as? String ?? "", deviceRating: data.value(forKey: "rating") as? String ?? "")
            return cell
    }
    
    
}
