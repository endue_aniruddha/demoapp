
import Foundation

class HomeViewModel: NSObject{
    var apiService: APIServices?
    var modelData: HomeModel?{
        didSet{
            apiHandlerData()
        }
    }
    var apiHandlerData:(()->())={}
    override init() {
        super.init()
        apiService = APIServices()
        getDataSource()
    }
    func getDataSource() {
        apiService?.callAPInetwork(escapingData: { (productdata) in
                                    DispatchQueue.main.async {
                                        self.modelData = productdata }})}
    
    func filterLowPrice() -> [products]? {
        let filterLowPrice = modelData?.products?.filter { $0.convertIntprice ?? 0 <= 1000}
        let priceLow = filterLowPrice
        return priceLow
    }
    
    func filterHighPrice() -> [products]? {
        let filterHighPrice = modelData?.products?.filter{$0.convertIntprice ?? 0 > 1000}
        let priceHigh = filterHighPrice
        return priceHigh
    }
    
}
